FROM docker.io/elastic/filebeat:8.11.3

COPY --chown=root:filebeat --chmod=644 filebeat.yml /usr/share/filebeat/filebeat.yml
COPY --chown=root:filebeat --chmod=644 zeek.yml /usr/share/filebeat/modules.d/zeek.yml

